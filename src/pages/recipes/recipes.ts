import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Recipes } from './../../models/recipe';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { EditRecipePage } from "../edit-recipe/edit-recipe";
import { RecipeService } from '../../sevice/recipe';
import { RecipePage } from '../recipe/recipe';
import { DatabaseOptionsPage } from '../databaseOptions/databaseOptions';
import { AuthService } from '../../sevice/authService';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
  selector: "page-recipes",
  templateUrl: "recipes.html"
})
export class RecipesPage {
  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              public recipeService: RecipeService,
              public loadingCtrl:LoadingController,
              public popoverCtrl :PopoverController,
              public authservice :AuthService,
              public alertCtrl :AlertController
            ) {}
    recipes : Recipes[];

    ionViewWillEnter(){
     this.recipes = this.recipeService.getRecipes();
    }
    onNewRecipe() {
    this.navCtrl.push(EditRecipePage,{ mode : 'New'});
  }
  onLoadRecipe(i : Number, recipe : Recipes){
    this.navCtrl.push(RecipePage, {i : i, recipe : recipe})
  }

  onShowOption($event : MouseEvent){
    const loading = this.loadingCtrl.create({
      content :"Please Wait ..."
    })
    const popover = this.popoverCtrl.create(DatabaseOptionsPage);
    popover.present({ev: event});
    popover.onDidDismiss(
      (data)=>{
        if(!data){
          return;
        }
        loading.present();
        if(data.action == "Load"){
          this.authservice.getActiveUser().getToken()
          .then(
            (token:string)=>{
              this.recipeService.fetchList(token)
              .subscribe(
                (list : Recipes[])=>{
                  loading.dismiss();
                  if(list){
                    this.recipes= list;
                  }else{
                    this.recipes = [];
                  }
                 console.log("ok")
                },
                (error) => {
                  loading.dismiss();
                  this.errorHandle(error.json().error);
                }
              );
            }
          );
        }else if (data.action == "Store"){
          loading.present();
          this.authservice.getActiveUser().getToken()
          .then(
            (token:string)=>{
              this.recipeService.storeList(token)
              .subscribe(
                ()=>{
                  console.log("Success");
                  loading.dismiss();
                },
                (error) =>{
                  this.errorHandle(error.json().error);
                  loading.dismiss();}
              )
            }
          )
        }
      }
    )
  }

  errorHandle(errorMessage : string){
    this.alertCtrl.create({
      title : "Error",
      message: errorMessage,
      buttons:["ok"]
    }).present();
  }
}
