import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { AuthService } from '../../sevice/authService';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public serviceCtrl :AuthService,
    public loadingCtrl : LoadingController,
    public alertCtrl : AlertController) {
  }

  onSignin(form:NgForm){
    const loading = this.loadingCtrl.create({
      content:"Signin..."
    });
    loading.present();
    this.serviceCtrl.signin(form.value.email, form.value.password)
    .then(data =>{
      loading.dismiss();
    })
    .catch(error =>{
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title:"Sign Failed",
        message:error.message,
        buttons:["Ok"]
      })
      alert.present();
    })
  }
}
