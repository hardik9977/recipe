import { LoadingController } from "ionic-angular/components/loading/loading-controller";
import { Ingredient } from "./../../models/ingredient.model";
import { ShoppingListService } from "./../../sevice/shoppinglist";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { NgForm } from "@angular/forms/src/directives/ng_form";
import { PopoverController } from "ionic-angular/components/popover/popover-controller";

import { AuthService } from "../../sevice/authService";
import { AlertController } from "ionic-angular/components/alert/alert-controller";
import { DatabaseOptionsPage } from "../databaseOptions/databaseOptions";

@IonicPage()
@Component({
  selector: "page-shopping-list",
  templateUrl: "shopping-list.html"
})
export class ShoppingListPage {
  constructor(
    public serviceCtrl: ShoppingListService,
    public popoverCtrl: PopoverController,
    public authservice: AuthService,
    public shoopinglistService: ShoppingListService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {}
  listItem: Ingredient[];
  onAddItem(form: NgForm) {
    this.serviceCtrl.addItem(form.value.IngredientName, form.value.amount);
    form.reset();
    this.ionViewWillEnter();
  }
  ionViewWillEnter() {
    this.listItem = this.serviceCtrl.getItems();
  }
  onCheckItem(index: number) {
    this.serviceCtrl.removeItem(index);
    this.ionViewWillEnter();
  }

  onShowOption($event: MouseEvent) {
    const loading = this.loadingCtrl.create({
      content: "Please Wait ..."
    });
    const popover = this.popoverCtrl.create(DatabaseOptionsPage);
    popover.present({ ev: event });
    popover.onDidDismiss(data => {
      if (!data) {
        return;
      }
      loading.present();
      if (data.action == "Load") {
        this.authservice
          .getActiveUser()
          .getToken()
          .then((token: string) => {
            this.shoopinglistService.fetchList(token).subscribe(
              (list: Ingredient[]) => {
                loading.dismiss();
                if (list) {
                  this.listItem = list;
                } else {
                  this.listItem = [];
                }
                console.log("ok");
              },
              error => {
                loading.dismiss();
                this.errorHandle(error.json().error);
              }
            );
          });
      } else if (data.action == "Store") {
        loading.present();
        this.authservice
          .getActiveUser()
          .getToken()
          .then((token: string) => {
            this.shoopinglistService.storeList(token).subscribe(
              () => {
                console.log("Success");
                loading.dismiss();
              },
              error => {
                this.errorHandle(error.json().error);
                loading.dismiss();
              }
            );
          });
      }
    });
  }

  errorHandle(errorMessage: string) {
    this.alertCtrl
      .create({
        title: "Error",
        message: errorMessage,
        buttons: ["ok"]
      })
      .present();
  }
}
