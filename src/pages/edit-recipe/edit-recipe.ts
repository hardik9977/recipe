import { Ingredient } from "./../../models/ingredient.model";
import { Component, OnInit } from "@angular/core";
import { IonicPage, NavParams } from "ionic-angular";
import { ActionSheetController } from "ionic-angular/components/action-sheet/action-sheet-controller";
import { AlertController } from "ionic-angular/components/alert/alert-controller";
import { FormGroup } from "@angular/forms";
import { FormControl, FormArray } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ToastController } from "ionic-angular/components/toast/toast-controller";
import { RecipeService } from "../../sevice/recipe";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { Recipes } from "../../models/recipe";

@Component({
  selector: "page-edit-recipe",
  templateUrl: "edit-recipe.html"
})
export class EditRecipePage implements OnInit {
  constructor(
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public recipeService: RecipeService,
    public navCtrl: NavController
  ) {}
  options = ["Easy", "Medium", "Difficult"];
  mode: String = "New";
  recipeForm: FormGroup;
  recipe: Recipes;
  index: number;
  ngOnInit() {
    this.mode = this.navParams.get("mode");
    if (this.mode == "Edit") {
      this.recipe = this.navParams.get("recipe");
      this.index = this.navParams.get("index");
    }
    this.initializeForm();
  }

  onManageIngredients() {
    const actionsheet = this.actionSheetCtrl.create({
      title: "What do you want",
      buttons: [
        {
          text: "Add Ingredients",
          handler: () => {
            this.createNewIngredientsAlert().present();
          }
        },
        {
          text: "Remove all Ingredients",
          role: "distructive",
          handler: () => {
            const fArray: FormArray = <FormArray>this.recipeForm.get(
              "ingredients"
            );
            for (let i = fArray.length - 1; i >= 0; i--) {
              fArray.removeAt(i);
            }
          }
        },
        {
          text: "Cancle",
          role: "cancle"
        }
      ]
    });
    actionsheet.present();
  }

  private createNewIngredientsAlert() {
    return this.alertCtrl.create({
      title: "Add Ingredient",
      inputs: [
        {
          name: "name",
          placeholder: "name"
        }
      ],
      buttons: [
        {
          text: "Add",
          handler: data => {
            if (data.name.trim() == "" || data.name == null) {
              this.toastCtrl
                .create({
                  message: "Please Enter Vaild Value!",
                  duration: 2000,
                  position: "top"
                })
                .present();
              return;
            }

            (<FormArray>this.recipeForm.get("ingredients")).push(
              new FormControl(data.name, Validators.required)
            );
            this.toastCtrl
              .create({
                message: "One Item Added!",
                duration: 2000,
                position: "top"
              })
              .present();
          }
        },
        {
          text: "Cancle",
          role: "cancle"
        }
      ]
    });
  }

  onSubmits() {
    const value = this.recipeForm.value;
    let ingredients: Ingredient[] = [];
    if (ingredients.length > 0) {
      ingredients = value.ingerdients.map(name => {
        return { name: name, amount: 1 };
      });
    }
    if (this.mode == "Edit") {
      this.recipeService.updateRecipe(
        this.index,
        value.title,
        value.description,
        value.difficulty,
        value.ingredients
      );
    } else {
      this.recipeService.addRecipe(
        value.title,
        value.description,
        value.difficulty,
        value.ingredients
      );
    }
    this.recipeForm.reset();
    this.navCtrl.popToRoot();
  }

  private initializeForm() {
    let title = null;
    let difficulty: String = "Medium";
    let discription = null;
    let ingredients = [];

    if (this.mode == "Edit") {
      title = this.recipe.title;
      difficulty = this.recipe.difficulty;
      discription = this.recipe.desciption;
      for (let ingredient of this.recipe.ingredients) {
        ingredients.push(new FormControl(ingredient));
      }
    }

    this.recipeForm = new FormGroup({
      title: new FormControl(title, Validators.required),
      description: new FormControl(discription, Validators.required),
      difficulty: new FormControl(difficulty, Validators.required),
      ingredients: new FormArray([])
    });
  }
}
