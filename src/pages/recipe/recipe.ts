import { EditRecipePage } from "./../edit-recipe/edit-recipe";
import { OnInit } from "@angular/core";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Recipes } from "../../models/recipe";
import { RecipeService } from "../../sevice/recipe";
import { ShoppingListService } from "../../sevice/shoppinglist";

@Component({
  selector: "page-recipe",
  templateUrl: "recipe.html"
})

export class RecipePage implements OnInit {
  constructor(public navCtrl: NavController, public navParams: NavParams, public recipeService : RecipeService,public shoppinglistService :ShoppingListService) {}
  recipe: Recipes;
  index: number;
  ngOnInit() {
    this.recipe = this.navParams.get("recipe");
    this.index = this.navParams.get("i");
    console.log(this.recipe.difficulty);
  }

  onAddIngredients() {
    this.shoppinglistService.addItems(this.recipe.ingredients);
  }

  onEditRecipe() {
    this.navCtrl.push(EditRecipePage, {
      mode: "Edit",
      recipe: this.recipe,
      index: this.index
    });
  }

  onDeleteRecipe() {
    this.recipeService.removeRecipe(this.index);
    this.navCtrl.popToRoot();
  }
}
