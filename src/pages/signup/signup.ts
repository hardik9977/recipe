import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { NgForm } from "@angular/forms/src/directives/ng_form";
import { AuthService } from "../../sevice/authService";
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Content } from 'ionic-angular/components/content/content';

@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthService,
    public loadingCtrl : LoadingController,
    public alertCtrl : AlertController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad SignupPage");
  }

  onSignup(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content : "Signup...."
    });
    loading.present();
    this.authService.signup(form.value.email, form.value.password)
      .then(data => {
        loading.dismiss();
  })
  .catch(error =>{
    const alert = this.alertCtrl.create({
      title : "Signup Failed",
      message : error.message,
      buttons:["Ok"]
    });
    loading.dismiss();
    alert.present();

  }) 
}
}
