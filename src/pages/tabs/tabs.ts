import { ShoppingListPage } from './../shopping-list/shopping-list';
import { Component } from '@angular/core';
import { RecipesPage } from '../recipes/recipes';
@Component({
    selector : "app-tabs",
    template : `<ion-tabs>
    <ion-tab [root]="shoppinglist"  tabIcon="list-box" tabTitle = "Shopping List"></ion-tab>
    <ion-tab [root]="recipe" tabIcon="book" tabTitle = "Recipe" ></ion-tab>
    </ion-tabs>`
})

export class TabsPage {
    shoppinglist =ShoppingListPage;
    recipe = RecipesPage
}