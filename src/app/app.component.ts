import { NavController } from "ionic-angular/navigation/nav-controller";
import { Component } from "@angular/core";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { TabsPage } from "../pages/tabs/tabs";
import { SigninPage } from "../pages/signin/signin";
import { SignupPage } from "../pages/signup/signup";
import { ViewChild } from "@angular/core";
import { MenuController } from "ionic-angular/components/app/menu-controller";
import * as firebase from 'firebase'
import { AuthService } from "../sevice/authService";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  tabspage: any = TabsPage;
  signinPage: any = SigninPage;
  signupPage: any = SignupPage;
  isAuthenticated:boolean =false;

  @ViewChild("nav") nav: NavController;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private authservice :AuthService
  ) {
    firebase.initializeApp(
      {
        apiKey: "AIzaSyD3s5x7iocZRYjFni_-6GiPNeyrfApTRG8",
        authDomain: "ionic2-recipebook-f20a2.firebaseapp.com",
      }
    );

    firebase.auth().onAuthStateChanged(user =>{
      if(user){
        this.isAuthenticated = true,
        this.nav.setRoot(this.tabspage);
      }
      else{
        this.isAuthenticated = false;
        this.nav.setRoot(this.signinPage);
      }
    })
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoad(page: any) {
    this.nav.push(page);
    this.menuCtrl.close();
  }

  onlogout() {
    this.authservice.signout();
    this.menuCtrl.close();
    this.nav.setRoot(this.signinPage);
  }
}
