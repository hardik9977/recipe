import { Ingredient } from "./ingredient.model";

export class Recipes {
    constructor (public title:String,
                 public desciption : String,
                 public difficulty : String,
                 public ingredients : Ingredient[]){}
}