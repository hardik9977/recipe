import { Ingredient } from './../models/ingredient.model';
import { Injectable } from '@angular/core';
import { AuthService } from './authService';
import { Http } from '@angular/http';
import { Response } from '@angular/http/src/static_response';
import 'rxjs/RX';

@Injectable()
export class ShoppingListService{
    private ingredients: Ingredient[]=[];

    constructor(private authservice : AuthService,
                private http:Http){}

    addItem(name:String,amount:Number){
        this.ingredients.push(new Ingredient(name,amount));
        console.log(this.ingredients);
    }

    addItems(items:Ingredient[])
    {
        console.log(items);
        this.ingredients.push(...items);   
    }

    getItems(){
        return this.ingredients.slice();
    }

    removeItem(index : number){
        this.ingredients.splice(index,1);
    }

    storeList(token:string){
        const userId = this.authservice.getActiveUser().uid;
        return this.http.put('https://ionic2-recipebook-f20a2.firebaseio.com/'+userId+'/shopping-list.json?auth='+token,
        this.ingredients)
        .map((responce:Response)=>{
            return responce.json();
        })
    }

    fetchList(token:string){
        const userId = this.authservice.getActiveUser().uid;
        return this.http.get('https://ionic2-recipebook-f20a2.firebaseio.com/'+userId+'/shopping-list.json?auth='+token)
        .map((responce : Response)=>{
            return responce.json();
        })
        .do((ingredients : Ingredient[]) =>{
            if(ingredients){
                this.ingredients = ingredients;
            }else{
                this.ingredients = [];
            }
        });
    }
}