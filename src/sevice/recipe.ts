import { Recipes } from './../models/recipe';
import { Response } from '@angular/http/src/static_response';
import { Http } from '@angular/http';
import { Recipes } from "../models/recipe";
import { Ingredient } from "../models/ingredient.model";
import { Injectable } from "@angular/core";
import { AuthService } from './authService';
import 'rxjs/Rx'
@Injectable()
export class RecipeService {
  public recipes: Recipes[] = [];
  constructor(private http :Http,
              private authservice :AuthService){}
  addRecipe(
    title: String,
    discription: String,
    difficulty: String,
    ingredients: Ingredient[]
  ) {
    this.recipes.push(new Recipes(title, discription, difficulty, ingredients));
    console.log(this.recipes)
  }

  getRecipes(){
   return this.recipes.slice();
  }

  updateRecipe(
    index:number,
    title: String,
    discription: String,
    difficulty: String,
    ingredients: Ingredient[]
  ){
    this.recipes[index] = new Recipes(title, discription, difficulty, ingredients);
  }

  removeRecipe(index:number){
    this.recipes.splice(index,1)
  }

  storeList(token:string){
    const userId = this.authservice.getActiveUser().uid;
    return this.http.put('https://ionic2-recipebook-f20a2.firebaseio.com/'+userId+'/recipe-list.json?auth='+token,
    this.recipes)
    .map((responce:Response)=>{
        return responce.json();
    })
}

fetchList(token:string){
    const userId = this.authservice.getActiveUser().uid;
    return this.http.get('https://ionic2-recipebook-f20a2.firebaseio.com/'+userId+'/recipe-list.json?auth='+token)
    .map((responce : Response)=>{
        const recipes : Recipes[] = responce.json() ? responce.json() : [];
        for(let item of recipes){
          if(!item.hasOwnProperty){
            item.ingredients = [];
          }
        }
        return recipes
    })
    .do((recipes : Recipes[]) =>{
        if(recipes){
          this.recipes = recipes;
        }else{
          this.recipes = [];
        }
    });
}
}
